package lab05Eclipse;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsChoice implements EventHandler<ActionEvent>{
	
	private TextField message;
	private TextField win;
	private TextField losses;
	private TextField ties;
	private String playerChoice;
	private RpsGame game;
	

	public RpsChoice(TextField message, TextField win, TextField losses, TextField ties, String playerChoice, RpsGame game) {
		
		this.message = message;
		this.win = win;
		this.losses = losses;
		this.ties = ties;
		this.playerChoice = playerChoice;
		this.game = game; 
		
	}
	
	//handle method will happen after an event such as clicking a button
	public void handle(ActionEvent e) {
		String result = this.game.playRound(this.playerChoice);
		//adding the contents of result to our textField message
		this.message = new TextField(result);
		//updating the counters
		String wins = Integer.toString(this.game.getNumberOfWins());//Here we use the Integer.toString() to convert the integer of numberOfWins from the RpsGame class to a String
		this.win = new TextField(wins);
		String losses = Integer.toString(this.game.getNumberOfLosses());
		this.losses = new TextField(losses);
		String ties = Integer.toString(this.game.getNumberOfTies());
		this.ties = new TextField(ties);
		
	}
	
	

}
