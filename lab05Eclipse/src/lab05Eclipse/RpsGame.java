/**
 * @author Kevin Echenique
 */
package lab05Eclipse;
import java.util.Random;

public class RpsGame {
	
	private int numberOfWins = 0;
	private int numberOfLosses = 0;
	private int numberOfTies = 0;
	//Making a Random object
	Random rand = new Random();
	
	//GET METHODS
	public int getNumberOfWins() {
		return this.numberOfWins;
	}
	public int getNumberOfLosses() {
		return this.numberOfLosses;
	}
	public int getNumberOfTies() {
		return this.numberOfTies;
	}
	
	//Randomly chooses a symbol that will represent either rock, paper or scissors and determines the winner
	public String playRound(String playerChoice) {
		//picks a random value from 0 to 2 which will represent one of the three symbols
		int computerChoice = rand.nextInt(3);
		String[] computerSymbol = {"rock", "paper", "scissors"};
		String matchResult;
		//case1: a tie
		if(computerSymbol[computerChoice].equals(playerChoice)) {
			matchResult = "Computer plays " + computerSymbol[computerChoice] + " and it was a tie!";
			this.numberOfTies += 1;
			return matchResult; 
		}
		//case2: computer wins
		else if(computerSymbol[computerChoice].equals("rock") && playerChoice.equals("scissors") || computerSymbol[computerChoice].equals("paper") && playerChoice.equals("rock") || computerSymbol[computerChoice].equals("scissors") && playerChoice.equals("paper")) {
			matchResult = "Computer plays " + computerSymbol[computerChoice] + " and the computer won! (you can do better than that) :(";
			this.numberOfLosses += 1;
			return matchResult;
		}
		else if(computerSymbol[computerChoice].equals("scissors") && playerChoice.equals("rock") || computerSymbol[computerChoice].equals("rock") && playerChoice.equals("paper") || computerSymbol[computerChoice].equals("paper") && playerChoice.equals("scissors")){
			matchResult = "Computer plays " + computerSymbol[computerChoice] + " and the computer lost! (what a loser) :)";
			this.numberOfWins += 1;
			return matchResult;
		}
		else {
			throw new IllegalArgumentException(playerChoice + " is not a valid option. Please try again.");
		}
	}
	

}
