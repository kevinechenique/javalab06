package lab05Eclipse;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application{
	
	//Creating an RpsGame object. Since it's a private field its scope will only last throughout the duration of the program
	//private RpsGame game = new RpsGame();
	
	public void start(Stage stage) {
		//creates the root of the scene
		Group root = new Group();
		
		//scene is associated with container, dimensions and is associated to root
		Scene scene = new Scene(root, 650, 300);
		scene.setFill(Color.BLACK);
		
		//Creating Buttons and TextFields and configuring the layout of these elements
		HBox buttons = new HBox();//creates a Horizontal box object to layout the Buttons
		HBox textFields = new HBox();//creates a Horizontal box object to layout the text fields
		VBox vb = new VBox();//creates a horizontal box object that will contain the HBox layout
		
		//creating Buttons and TextFields
		Button rock = new Button("rock");
		Button paper = new Button("paper");
		Button scissors = new Button("scissors");
		TextField greet = new TextField("Welcome!");
		TextField wins = new TextField("wins: ");
		TextField losses = new TextField("losses: ");
		TextField ties = new TextField("ties: ");
		//Changing the width of the text field elements to 200 pixels
		greet.setPrefWidth(200);
		wins.setPrefWidth(200);
		losses.setPrefWidth(200);
		ties.setPrefWidth(200);
		
		
		//making a RpsGame object
		RpsGame game = new RpsGame();
		//making a RpsChoice object for every button (choice). RpsChoice has the event handler
		RpsChoice chooseRock = new RpsChoice(greet, wins, losses, ties, "rock", game);
		RpsChoice choosePaper = new RpsChoice(greet, wins, losses, ties, "paper", game);
		RpsChoice chooseScissors = new RpsChoice(greet, wins, losses, ties, "scissors", game);
		//Setting the on action command for every button
		rock.setOnAction(chooseRock);
		paper.setOnAction(choosePaper);
		scissors.setOnAction(chooseScissors);
		
		
		//Adding the button and text fields as children of a horizontal box
		buttons.getChildren().addAll(rock, paper, scissors);
		textFields.getChildren().addAll(greet, wins, losses, ties);
		
		//adding both, the buttons and text fields to the same vertical box
		//vb.getChildren().add(buttons); //.add is used when there is only a single node to add
		vb.getChildren().addAll(buttons, textFields);//.addAll is used when there are more than one node to add
		
		//associate scene to stage and show. it basically sets what we can see (read)
		stage.setTitle("Rock Paper Scissors");
		stage.setScene(scene);
		
		stage.show();
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}

}
